package io.gripxtech.odoojsonrpcclient.core.utils.recycler.entities

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerLessBinding
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerMoreBinding

class MoreViewHolder(var viewBinding: ItemViewRecyclerMoreBinding) : RecyclerView.ViewHolder(viewBinding.root)
