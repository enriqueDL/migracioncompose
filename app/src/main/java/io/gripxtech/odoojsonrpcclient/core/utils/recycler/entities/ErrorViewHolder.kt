package io.gripxtech.odoojsonrpcclient.core.utils.recycler.entities

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerEmptyBinding
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerErrorBinding

class ErrorViewHolder(var viewBinding: ItemViewRecyclerErrorBinding) : RecyclerView.ViewHolder(viewBinding.root)
