package io.gripxtech.odoojsonrpcclient.core.utils.recycler.entities

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerErrorBinding
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerLessBinding

class LessViewHolder(var viewBinding: ItemViewRecyclerLessBinding) : RecyclerView.ViewHolder(viewBinding.root)
