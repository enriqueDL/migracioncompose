package io.gripxtech.odoojsonrpcclient.core.utils.recycler.entities

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewRecyclerEmptyBinding

class EmptyViewHolder(var viewBinding: ItemViewRecyclerEmptyBinding) : RecyclerView.ViewHolder(viewBinding.root)
