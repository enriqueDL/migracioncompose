package io.gripxtech.odoojsonrpcclient.core.authenticator

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewManageAccountBinding

class ManageAccountViewHolder(var viewBinding: ItemViewManageAccountBinding) : RecyclerView.ViewHolder(viewBinding.root)
