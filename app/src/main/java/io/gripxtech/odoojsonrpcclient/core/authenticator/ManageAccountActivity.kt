package io.gripxtech.odoojsonrpcclient.core.authenticator

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatDelegate
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.*
import io.gripxtech.odoojsonrpcclient.core.utils.BaseActivity
import io.gripxtech.odoojsonrpcclient.core.utils.recycler.decorators.VerticalLinearItemDecorator
import io.gripxtech.odoojsonrpcclient.databinding.ActivityManageAccountBinding
import io.reactivex.disposables.CompositeDisposable


class ManageAccountActivity : BaseActivity() {

    companion object {
        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        }
    }

    lateinit var app: App private set
    lateinit var glideRequests: GlideRequests
    var compositeDisposable: CompositeDisposable? = null
        private set
    lateinit var adapter: ManageAccountAdapter private set
    private lateinit var binding: ActivityManageAccountBinding
    private lateinit var view: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = application as App
        binding = ActivityManageAccountBinding.inflate(layoutInflater)
        view = binding.root
        glideRequests = GlideApp.with(this)
        compositeDisposable?.dispose()
        compositeDisposable = CompositeDisposable()
        setContentView(view)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val users = getOdooUsers()
        val layoutManager = LinearLayoutManager(
            this, RecyclerView.VERTICAL, false
        )
        binding.rv.layoutManager = layoutManager
        binding.rv.addItemDecoration(
            VerticalLinearItemDecorator(
                resources.getDimensionPixelOffset(R.dimen.default_8dp)
            )
        )

        adapter = ManageAccountAdapter(this, ArrayList(users), binding)
        binding.rv.adapter = adapter
    }

    override fun onDestroy() {
        compositeDisposable?.dispose()
        super.onDestroy()
    }
}
