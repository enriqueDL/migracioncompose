package io.gripxtech.odoojsonrpcclient.core.authenticator

import android.os.Bundle
import android.util.Base64
import android.view.View
import io.gripxtech.odoojsonrpcclient.*
import io.gripxtech.odoojsonrpcclient.core.utils.BaseActivity
import io.gripxtech.odoojsonrpcclient.databinding.ActivityManageAccountBinding
import io.gripxtech.odoojsonrpcclient.databinding.ActivityProfileBinding


class ProfileActivity : BaseActivity() {

    private lateinit var app: App
    lateinit var glideRequests: GlideRequests
    private lateinit var binding: ActivityProfileBinding
    private lateinit var view: View

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        app = application as App
        binding = ActivityProfileBinding.inflate(layoutInflater)
        view = binding.root
        glideRequests = GlideApp.with(this)
        setContentView(view)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        val user = getActiveOdooUser()
        if (user != null) {
            val imageSmall = user.imageSmall.trimFalse()
            val name = user.name.trimFalse()

            glideRequests.asBitmap().load(
                if (imageSmall.isNotEmpty())
                    Base64.decode(imageSmall, Base64.DEFAULT)
                else
                    app.getLetterTile(if (name.isNotEmpty()) name else "X")
            ).circleCrop().into(binding.ivProfile)

            binding.ctl.title = name
            binding.tvName.text = name
            binding.tvLogin.text = user.login
            binding.tvServerURL.text = user.host
            binding.tvDatabase.text = user.database
            binding.tvVersion.text = user.serverVersion
            binding.tvTimezone.text = user.timezone
        }
    }
}
