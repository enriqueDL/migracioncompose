package io.gripxtech.odoojsonrpcclient.customer

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.gripxtech.odoojsonrpcclient.databinding.ItemViewCustomerBinding

class CustomerViewHolder(var viewBinding: ItemViewCustomerBinding) : RecyclerView.ViewHolder(viewBinding.root)
